<footer>
  <div class="footer-container container ">
    <div class="footer-inner">
    <h3>PengDrive</h3>&nbsp&copy;2018
    </div>
    <div  class="footer-inner">
      <h3>Powered by</h3> &nbsp
      <a href=" http://www.raspberrypi.org" target="_blank"><img src="images/icons/RPi-Logo-Landscape-Reg-SCREEN.png" alt="raspberry pi logo and link"></a>
    </div>
  </div>
 </footer>
 <script src="js/rank_animation.js"></script>
</body>
</html>