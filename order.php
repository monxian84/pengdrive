<?php 
require 'header.php';

?>
<div class="order-form container">
  <form action="order_submit.php" method="post">
    <div class="form-items">

      <div class="inp-row">
        <div class="inp-row-item">

          <label for="distro">Pick a Distro<sup>*</sup></label>
          <?php 
              $f_pointer=fopen("rank.txt", "r"); //file pointer
              $ar = [];
              while( ! feof($f_pointer)){
              $ar=fgetcsv($f_pointer,0, "^");
              echo '<input type="text" list="distros" placeholder="click to see top 10"/>';
              echo '<datalist id="distros">';
                for($i=0; $i<9; $i++) {
                  $name = ucfirst($ar[$i]);
                  echo '<option value="'.$name.'">#'.($i+1) .' ' .$name;;
              
                }
              }
              echo '</datalist>';
            ?>

        </div>
        <div class="inp-row-item">
          <label for="color">Color<sup>*</sup></label>
          <select name="color" id="color">
            <option value="white">White</option>
            <option value="blue">Blue</option>
            <option value="black">Black</option>
            <option value="red">Red</option>
          </select>
        </div>

      </div>

      <label for="firstname">First Name<sup>*</sup></label>
      <input type="text" id="firstname" placeholder="first name..." />

      <label for="lastname">Last Name<sup>*</sup></label>
      <input type="text" id="lastname" placeholder="last name..." />

      <label for="email">Email Address<sup>*</sup></label>
      <input type="email" id="email" placeholder="email..." />

      <label for="address">Street Address<sup>*</sup></label>
      <input type="text" id="address" placeholder="address..." />

      <div class="inp-row">
        <div class="inp-row-item">
          <label for="city">Street city<sup>*</sup></label>
          <input type="text" id="city" placeholder="city..." />
        </div>
        <div class="inp-row-item">
          <label for="state">State/Province/Region<sup>*</sup></label>
          <input type="text" id="state" placeholder="state..." />
        </div>
        <div class="inp-row-item">
          <label for="zipcode">Zip/Postal Code<sup>*</sup></label>
          <input type="text" id="zipcode" placeholder="zip code..." />
        </div>
      </div>

      <label for="country">Country<sup>*</sup></label>
      <input type="text" id="country" placeholder="country..." />

      <input type="submit" value="Submit" />

    </div>
  </form>
</div>