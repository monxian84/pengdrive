import requests
from bs4 import BeautifulSoup
import re
import os.path

save_path = '/mymirror/bitlamp/apps/pengdrive/htdocs'

response = requests.get("https://distrowatch.com")
doc = response.text
soup = BeautifulSoup(doc, "lxml")

table = soup.find("table", class_ = "Logo")

second_table_tag = table.tr.td.next_sibling.next_sibling
third_table_tag = second_table_tag.next_sibling.next_sibling

#gets rid of newline characters
new_distros = [tag for tag in second_table_tag.table.contents if tag != "\n"]


distroValue = (new_distros[2].find("img")["src"])+'^'
distroValue +=(new_distros[2].find("img")["title"])+'^'
distroValue +=(new_distros[2].find("td", class_ = "NewsText").get_text())+'^'

distroValue +=(new_distros[3].find("img")["src"])+'^'
distroValue +=(new_distros[3].find("img")["title"])+'^'
distroValue +=(new_distros[3].find("td", class_ = "NewsText").get_text())+'^'

distroValue +=(new_distros[4].find("img")["src"])+'^'
distroValue +=(new_distros[4].find("img")["title"])+'^'
distroValue +=(new_distros[4].find("td", class_ = "NewsText").get_text())+'^'


completeName = os.path.join(save_path, 'distros.txt')  
file = open(completeName,'w')
file.write(distroValue)
file.close()

# tr tags in the table
# print(new_distros[2].find("img")["src"])+'^' # this is the 3rd tr tag
# print(new_distros[2].find("img")["title"]) # this is the 3rd tr tag
# print(new_distros[2].find("td", class_ = "NewsText").get_text()) # this is the 3rd tr tag
# print(new_distros[3].find("img")["src"])
# print(new_distros[3].find("img")["title"])
# print(new_distros[3].find("td", class_ = "NewsText").get_text()) # this is the 3rd tr tag
# print(new_distros[4].find("img")["src"])
# print(new_distros[4].find("img")["title"])
# print(new_distros[4].find("td", class_ = "NewsText").get_text()) # this is the 3rd tr tag

distro_name = ''
rank_distros = third_table_tag.find("table", class_ = "News", style = "direction: ltr")
rank_distros = [tag for tag in rank_distros.contents if tag != "\n"]
for i in range(3, 14):
  rank = rank_distros[i].find("th").get_text()

  #distro_name += rank_distros[i].find("a").get_text()+'^'
  rankhref = rank_distros[i].find("a").get('href')
  distro_name += rankhref.replace('?frphr','')+'^'
  
  
completeRank = os.path.join(save_path, 'rank.txt') 
print(rank + " : " + distro_name)
file = open(completeRank,'w')
file.write(distro_name)
file.close()