<section class="bg-blue-fade ">
  <div class="container sec-a">
   
    <div class="showcase-desktop">
      <div class="upper-banner">
        <h1>Desktop</h1>
      </div>
      <div class="showcase-desktop-forground"></div>
      <div class="cloud1"></div>
      <div class="cloud2"></div>
      <div class="cloud3"></div>
      <div class="skydive"></div>
    </div>
  </div>
  <div class="container-row banner text-white">
    <h2>LINUX Operating System</h2>
    <p>Pre-installed on usb thumbdrives.</p>
  </div>
</section>
