<?php
   require 'functions.php';
   require 'header.php';
   require 'hero.php'; 
 ?>
<section class="container sec-b">
  <div class="sec-b-left">
    <a href="order.php">
      <img src="images/icons/chute.svg" width="100" alt="">
    </a>
    <h2><a href="order.php">ORDER RIGHT NOW!</a></h2>

  </div>
  <div class="sec-b-right">
    <h2><a href="order.php"> Grab your chute!</a></h2>
    <p><strong>Let's go. Order right now... <b>Why?</b></strong></p>
    <p>Because you're an impluse buyer.</p>
    <p>It's fun to get things in the mail.</p>
  </div>
</section>
<section class="container sec-c">
  <div class="sec-c-items" id="card1-hover">
    <div id="sec-c-card1" class="sec-basis"></div>
    <h3>Your Choice</h3>
    <p>Pick any linux distribution. We can put it on a usb drive for you.</p>
  </div>
  <div class="sec-c-items" id="card2-hover">
    <div id="sec-c-card2" class="sec-basis"></div>
    <h3>Usb Drive</h3>
    <p>Usb drives imported directly from China. Hand selected for purity and durability.</p>
  </div>
  <div class="sec-c-items" id="card3-hover">
    <div id="sec-c-card3" class="sec-basis"></div>
    <h3>Shipping</h3>
    <p>We ship world wide. Free shipping on all orders over $20.</p>
  </div>
</section>

<section class="bg-blue-fade ">
  <div class="container-row banner sec-d">
    <h3 class="molle text-white">Latest Releases and News</h3>
    <p><i>from distrowatch</i></p>

    <div class="container cards">
      <?php 
        $f_pointer=fopen("distros.txt", "r"); //file pointer
        $ar = [];
        while( ! feof($f_pointer)){
        $ar=fgetcsv($f_pointer,0, "^");
       
        }

        for($i=0; $i<3; $i++) {
          $title = $ar[$i*3+1];
          $image = $ar[$i*3];
          $text = $ar[$i*3+2];
          $text = truncate($text, 300);

          if($ar[$i*3+1] == "DistroWatch Weekly"){
            $name = "weekly.php";
          } else {
            $name = $title;
          }
          echo '<div class="card ">';
          echo '<div class="card-inner"><a href="https://distrowatch.com/'. $name .'" target="_blank">';

          echo  '<img src="https://distrowatch.com/'.$image .' " width="100" height="100" alt=""></a>';
          echo '<div><p class="card-content">'.$text .'</p><a href="https://distrowatch.com/'. $name .'" target="_blank">Read More...</a></div>';
          echo '</div></div>';
        }
    ?>
    </div>
  </div>
</section>

<section class="container-row banner sec-e">

  <h3 class="molle ">Trending</h3>
  <p><i>also from distrowatch</i></p>

  <div class="ranking-container">
    <div class="ranking">
      <div class="top-row">
        <div class="igloo-rank"> </div>
        <div class="moving-peng"></div>
      </div>
      <div class="top-ten">
        <?php 
            $f_pointer=fopen("rank.txt", "r"); //file pointer
            $ar = [];
            while( ! feof($f_pointer)){
            $ar=fgetcsv($f_pointer,0, "^");
          
              for($i=0; $i<10; $i++) {
                $name = $ar[$i];
                echo '<div class="rank-items" id="rank'. ($i+1) .'"><h6>#'. ($i+1) .'</h6>';
                echo '<a href="https://distrowatch.com/table.php?distribution='.  strtolower($name) .'" target="_blank">';
                echo '<img src="https://distrowatch.com/images/yvzhuwbpy/'. strtolower($name) .'.png" width="60"></a>';
                echo '</div>';
              }
            }
          ?>
      </div>
    </div>
  </div>

</section>

<?php require 'footer.php'; ?>
</body>

</html>