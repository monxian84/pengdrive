<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
    crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Molle:400i|Oswald|Raleway" rel="stylesheet">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css">
  <title>PengDrive</title>
</head>

<body>
  <header>
    <div class="container">
      <div class="header-left">
        <img src="images/icons/pendive_logo1.png" alt="" width="50" height="50">
        <h2><a href="index.php">PengDrive</a></h2>
      </div>
      <div class="header-right">
        <div class="search-login-reg">
          <div class="box">
            <div class="form-container">
              <span class="icon"><i class="fa fa-search"></i></span>
              <input type="search" id="search" placeholder="Search..." />
            </div>
          </div>
          <div class="cart">
            <a href="cart.html">
              <img src="images/icons/shopping-cart.svg" alt="" width="30px" height="30px">
              <span>Items:0</span>
              <span>$0.00</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </header>